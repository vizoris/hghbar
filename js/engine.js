
$(function() {


// Поиск
$('.form-search__click').click(function() {
  $(this).removeClass('active');
  $('.form-search').fadeToggle();
  $('.page').addClass('opacity');
})
$(document).mouseup(function (e){ // событие клика по веб-документу
  var div = $(".form-search"); // тут указываем ID элемента
  if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) { // и не по его дочерним элементам
    div.hide(); // скрываем его
    $('.form-search__click').addClass('active');
    $('.page').removeClass('opacity');
  }
});

// слайдер карточки товара
var productSlider = $('.product-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.product-slider__nav'
});


$('.product-slider__nav').slick({
    slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.product-slider',
  dots: false,
  arrows: false,
  focusOnSelect: true,
  vertical: true,
  responsive: [
    {
      breakpoint: 1020,
      settings: {
        vertical: false
      }
    },
  ]
});

// Стилизация селектов
$('select').styler();


// Выпадающее меню
if ($(window).width() >= 992) {
   $('.drop').hover(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}else {
  $('.drop').click(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}


// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.header').toggleClass("active");
  $('.main-menu').fadeToggle(200);
});



//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});


// Фиксированный чек
 window.onload = function() { 
      $('.airSticky').airStickyBlock();
  };


// FansyBox
 $('.fancybox').fancybox({});




// Сллайдер авто
$('.auto-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});



// Меню в футере
if ($(window).width() <= 1020) {
  $('.footer-links__item:not(.footer-contacts) h4').click(function() {
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}


})